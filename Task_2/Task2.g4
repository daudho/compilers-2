grammar Task2;

r     : expr+ { System.out.println (" ");} //New line
      ;

expr  : expr '+' term { System.out.print ("+ "); }
      | expr '-' term { System.out.print ("- "); }
      | term
      ;

term  : term '*' factor { System.out.print ("* "); }
      | term '/' factor { System.out.print ("/ "); }
      | factor
      ;

factor      : '(' expr ')'
            | NUM { System.out.print ($NUM.text + " ") ;}
            | ID { System.out.print ($ID.text + " ");}
            ;

NUM   : [0-9]+ ;
ID    : [a-z]+ ;
WS    : [ \t\r\n]+ -> skip;