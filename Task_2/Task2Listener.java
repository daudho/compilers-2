// Generated from Task2.g4 by ANTLR 4.9.2
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link Task2Parser}.
 */
public interface Task2Listener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link Task2Parser#r}.
	 * @param ctx the parse tree
	 */
	void enterR(Task2Parser.RContext ctx);
	/**
	 * Exit a parse tree produced by {@link Task2Parser#r}.
	 * @param ctx the parse tree
	 */
	void exitR(Task2Parser.RContext ctx);
	/**
	 * Enter a parse tree produced by {@link Task2Parser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(Task2Parser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link Task2Parser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(Task2Parser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link Task2Parser#term}.
	 * @param ctx the parse tree
	 */
	void enterTerm(Task2Parser.TermContext ctx);
	/**
	 * Exit a parse tree produced by {@link Task2Parser#term}.
	 * @param ctx the parse tree
	 */
	void exitTerm(Task2Parser.TermContext ctx);
	/**
	 * Enter a parse tree produced by {@link Task2Parser#factor}.
	 * @param ctx the parse tree
	 */
	void enterFactor(Task2Parser.FactorContext ctx);
	/**
	 * Exit a parse tree produced by {@link Task2Parser#factor}.
	 * @param ctx the parse tree
	 */
	void exitFactor(Task2Parser.FactorContext ctx);
}