grammar Task;

// Need to call recursive rule expr from non-recursive rule
r     : expr+ { System.out.println ($expr.text + ".");};

// ANTLR4 : Left recursion!
// Operator precedence matches order of definition
expr  : '-' expr                // Unary minus
      | expr ('*' | '/' ) expr
      | expr ('+' | '-' ) expr
      | '(' expr ')'
      | INT
      | ID
      ;

INT   : [0-9]+ ;
ID    : [a-z]+ ;
WS    : [ \t\r\n]+ -> skip;