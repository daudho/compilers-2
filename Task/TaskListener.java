// Generated from Task.g4 by ANTLR 4.9.2
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link TaskParser}.
 */
public interface TaskListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link TaskParser#r}.
	 * @param ctx the parse tree
	 */
	void enterR(TaskParser.RContext ctx);
	/**
	 * Exit a parse tree produced by {@link TaskParser#r}.
	 * @param ctx the parse tree
	 */
	void exitR(TaskParser.RContext ctx);
	/**
	 * Enter a parse tree produced by {@link TaskParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(TaskParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link TaskParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(TaskParser.ExprContext ctx);
}