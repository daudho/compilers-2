grammar Hello;
r : 'Hello' ID { System.out.println("Greetings " + $ID.text + "."); } ;
ID : [a-zA-Z]+ ;
WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines, \r (Windows)