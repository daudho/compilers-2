import java.io.FileInputStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
public class Compiler {
  public static void main(String[] args) throws Exception {
    ANTLRInputStream input = new ANTLRInputStream(new FileInputStream(args[0]));
    HelloLexer lexer = new HelloLexer(input);
    HelloParser parser = new HelloParser(new CommonTokenStream(lexer));
    ParseTree tree = parser.r(); // begin parsing at rule ’r’
    System.out.println(tree.toStringTree(parser)); // print parse tree
  }
}